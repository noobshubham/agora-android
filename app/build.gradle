apply plugin: 'com.android.application'
apply plugin: 'kotlin-android'
apply plugin: 'kotlin-kapt'
apply plugin: "androidx.navigation.safeargs"
apply plugin: 'com.google.gms.google-services'
apply plugin: 'com.google.firebase.crashlytics'
apply plugin: 'kotlin-parcelize'

Properties properties = new Properties()
properties.load(project.rootProject.file('local.properties').newDataInputStream())
def fbApiScheme = properties.getProperty('fbApiScheme')
def fbApiKey = properties.getProperty('fbApiKey')
def secretKey = properties.getProperty('secretKey')
def serverKey = properties.getProperty('serverKey')

android {
  def keystoreProperties = new Properties()
  def keystorePropertiesFile = rootProject.file('signing.properties')
  if (keystorePropertiesFile.exists()) {
    keystoreProperties.load(new FileInputStream(keystorePropertiesFile))
  }

  lintOptions {
    baseline file("lint-baseline.xml")
  }
  compileSdkVersion rootProject.ext.compileSdk
  defaultConfig {
    resValue("string", "fbApiKey", fbApiKey)
    resValue("string", "fbApiScheme", fbApiScheme)
    resValue("string", "secretKey", secretKey)
    resValue("string", "serverKey", serverKey)
    vectorDrawables.useSupportLibrary = true
    applicationId "org.aossie.agoraandroid"
    minSdkVersion rootProject.ext.minSdk
    targetSdkVersion targetSdk
    versionCode 1
    versionName "1.0"
    testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
  }
  signingConfigs {
    file(rootProject.file('signing.properties')).with { propFile ->
      if (propFile.canRead()) {
        release {
          keyAlias keystoreProperties['keyAlias']
          keyPassword keystoreProperties['keyPassword']
          storeFile file(keystoreProperties['storeFile'])
          storePassword keystoreProperties['storePassword']
        }
      } else {
        print('not signed')
      }
    }
  }
  buildTypes {
    release {
      file(rootProject.file('signing.properties')).with { propFile ->
        if (propFile.canRead()) {
          signingConfig signingConfigs.release
        }
      }

      shrinkResources true
      minifyEnabled true
      proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
    }
    debug {
      debuggable true
    }
  }
  buildFeatures {
    viewBinding true
  }
  kotlinOptions {
    jvmTarget = JavaVersion.VERSION_1_8.toString()
  }
  compileOptions {
    sourceCompatibility JavaVersion.VERSION_1_8
    targetCompatibility JavaVersion.VERSION_1_8
  }
}

dependencies {
  implementation fileTree(dir: 'libs', include: ['*.jar'])
  implementation "androidx.appcompat:appcompat:$appCompatVersion"
  implementation "androidx.constraintlayout:constraintlayout:$constraintLayoutVersion"
  implementation "com.google.firebase:firebase-messaging:$firebaseMessagingVersion"
  testImplementation "junit:junit:$junitVersion"
  androidTestImplementation "androidx.test:runner:$testRunnerVersion"
  androidTestImplementation "androidx.test.espresso:espresso-core:$espressoCoreVersion"
  // Lifecycle components
  implementation "androidx.lifecycle:lifecycle-extensions:$lifecycleVersion"
  implementation "androidx.lifecycle:lifecycle-common-java8:$lifecycleVersion"
  //Design Support Library
  implementation "com.google.android.material:material:$materialDesignVersion"
  //Library for CardView
  implementation "androidx.cardview:cardview:$cardViewVersion"
  //Retrofit
  implementation "com.squareup.retrofit2:retrofit:$retrofitVersion"

  //Navigation Architecture Dependencies
  implementation "androidx.navigation:navigation-fragment-ktx:$navFragmentVersion"
  implementation "androidx.navigation:navigation-ui-ktx:$navUiVersion"
  //Shimmer
  implementation "com.facebook.shimmer:shimmer:$facebookShimmerVersion"
  //Facebook Login
  implementation "com.facebook.android:facebook-login:$facebookLoginVersion"
  implementation "com.facebook.android:facebook-android-sdk:$facebookAndroidSDK"
  //Pie Chart
  implementation "com.github.PhilJay:MPAndroidChart:v3.1.0"
  //MockWebServer and Roboelectric
  testImplementation "org.robolectric:robolectric:$robolectricVersion"
  testImplementation "com.squareup.okhttp3:mockwebserver:$mockWebServerVersion"
  // Mockito
  testImplementation "org.mockito:mockito-core:$mockitoCoreVersion"
  testImplementation "org.jetbrains.kotlinx:kotlinx-coroutines-test:$kotlinCoroutineTest"
  testImplementation "org.json:json:$jsonVersion"
  //kotlin deps
  implementation "androidx.core:core-ktx:$coreVersion"
  implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:$lifecycleViewmodelVersion"
  //dagger
  implementation "com.google.dagger:dagger:$daggerVersion"
  kapt "com.google.dagger:dagger-compiler:$daggerVersion"
  //Shared Prefs
  implementation "androidx.preference:preference-ktx:$prefsVersion"
  // room
  implementation "androidx.room:room-runtime:$roomVersion"
  kapt "androidx.room:room-compiler:$roomVersion"
  implementation "androidx.room:room-ktx:$roomVersion"
  // coroutine
  implementation "org.jetbrains.kotlinx:kotlinx-coroutines-android:$coroutineVersion"
  implementation "org.jetbrains.kotlinx:kotlinx-coroutines-core:$coroutinesCoreVersion"
  //horizontal calendar
  implementation "devs.mulham.horizontalcalendar:horizontalcalendar:$calendarVersion"
  //day view lib
  implementation "com.linkedin.android.tachyon:tachyon:$tachyonVersion"
  //picasso
  implementation "com.squareup.picasso:picasso:$picassoVersion"
  //timber
  implementation "com.jakewharton.timber:timber:$timberVersion"
  //httpLoggingInterceptor
  implementation "com.squareup.okhttp3:logging-interceptor:$httpLoggingInterceptor"
  //chucker
  debugImplementation "com.github.chuckerteam.chucker:library:$chuckerVersion"
  releaseImplementation "com.github.chuckerteam.chucker:library-no-op:$chuckerVersion"

  //leakcanary
  debugImplementation "com.squareup.leakcanary:leakcanary-android:$leakcanaryVersion"

  //Moshi
  implementation("com.squareup.moshi:moshi:$moshiVersion")
  kapt("com.squareup.moshi:moshi-kotlin-codegen:$moshiVersion")
  implementation("com.squareup.retrofit2:converter-moshi:$moshiConverterVersion")

  //recyclerView
  implementation "androidx.recyclerview:recyclerview:$recyclerViewVersion"

  //POI
  implementation "com.github.SUPERCILEX.poi-android:poi:$poi"

  //DataStore
  implementation("androidx.datastore:datastore-preferences:$dataStore")

  //Spotlight
  implementation "com.github.takusemba:spotlight:$spotlight"

  //FirebaseBoM
  implementation platform("com.google.firebase:firebase-bom:$firebaseBOMVersion")

  //Firebase Messaging
  implementation "com.google.firebase:firebase-messaging-ktx"

  //Firebase Analytics
  implementation "com.google.firebase:firebase-analytics-ktx"

  //Firebase Crashlytics
  implementation "com.google.firebase:firebase-crashlytics-ktx"

  //Biometric
  implementation "androidx.biometric:biometric:$biometricLibraryVersion"
  // Compose
  implementation "androidx.compose.runtime:runtime:$rootProject.composeVersion"
  implementation "androidx.compose.ui:ui:$rootProject.composeVersion"
  implementation "androidx.compose.foundation:foundation:$rootProject.composeVersion"
  implementation "androidx.compose.foundation:foundation-layout:$rootProject.composeVersion"
  implementation "androidx.compose.material:material:$rootProject.composeVersion"
  implementation "androidx.compose.runtime:runtime-livedata:$rootProject.composeVersion"
  implementation "androidx.compose.ui:ui-tooling-preview:$rootProject.composeVersion"
  debugImplementation "androidx.compose.ui:ui-tooling:$rootProject.composeVersion"
}
repositories {
  mavenCentral()
}
